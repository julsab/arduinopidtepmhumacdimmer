#include <DHT.h>

#define DHTPIN 12     // what pin we're connected to
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

bool humiditySensor_logEnabled = true;
void humiditySensor_setLogEnabled(bool logging) {
  humiditySensor_logEnabled = logging;
}

void humiditySensor_setup() {
  dht.begin();
}

float humiditySensor_humidity() {
  float adjustment = -10; // just stupid calibration 

  if (humiditySensor_logEnabled) {
    //Serial.println("humiditySensor: Read humidity");
  }
  float humidity = dht.readHumidity() + adjustment;
  
  //Print temp and humidity values to serial monitor
  if (humiditySensor_logEnabled) {
   Serial.print(" Humidity :  ");
   Serial.println(humidity);
   Serial.print(";");
  }
  
  return humidity;
}
