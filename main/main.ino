//include library
#include <LiquidCrystal.h>
#include <TimerOne.h>  // timer for AC dimmer
#include <PID_v1.h>

//defines
#define zero_interrupt 2
#define LCD_interupt 3 
#define triac 11 // Output to Opto Triac //AC_pin

//global variable//
float TEMP;
float SetTEMP = 37.5;
float Humidity;
// Navigation button variables
int readKey;
/* independant timed events in milis*/
const unsigned long evenTime_1_Temp = 596; 
const unsigned long evenTime_2_Hum =  597; 
const unsigned long evenTime_3_PID =  598;
const unsigned long evenTime_4_Display = 599; 

unsigned long previousTime_1 = 0;
unsigned long previousTime_2 = 0;
unsigned long previousTime_3 = 0;
unsigned long previousTime_4 = 0;
unsigned long previousTime_Settings = 0;


unsigned long SettingsTime = 10000;

//Variable for ac control//
volatile int i=0;               // Variable to use as a counter volatile as it is in an interrupt
volatile boolean zero_cross=0;  // Boolean to store a "switch" to tell us if we have crossed zero
volatile int dim = 0;                    // Dimming level (0-128)  0 = off, 128 = on
int inc=1;                      // counting up or down, 1=up, -1=down
  // Use the TimerOne Library to attach an interrupt
  // to the function we use to check to see if it is 
  // the right time to fire the triac.  This function 
  // will now run every freqStep in microseconds. 
int freqStep = 75;
 // PID PID PID// 
//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
//double Kp=2, Ki=5, Kd=1;
//double Kp=8, Ki=0.5, Kd=2;
//double Kp=20, Ki=0.5, Kd=2;// too muuch Kp
//double Kp=14, Ki=0.3, Kd=3; 
//double Kp=14, Ki=0.2, Kd=3; 
//double Kp=10, Ki=0.2, Kd=3;
//double Kp=6, Ki=0.1, Kd=3;
//double Kp=4, Ki=0.1, Kd=3;
double Kp=4, Ki=0.05, Kd=3;
  //Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);
double PIDmin = 10;
double PIDmax = 200;
 
  
  
// Setting the LCD shields pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

void setup() {
  thermometer_setup(); //DS18B20 instalization
  humiditySensor_setup(); // DT11  instalization
  //PID
  //initialize the variables we're linked to
   Input = TEMP;
   Setpoint = SetTEMP;
   
  //turn the PID on
  myPID.SetMode(AUTOMATIC);
  //myPID.SetMode(MANUAL);
  myPID.SetOutputLimits (PIDmin, PIDmax);
  // Initializes and clears the LCD screen
  lcd.begin(16, 2);
  lcd.clear();
  
  pinMode(triac, OUTPUT);
  
  
  Timer1.initialize(freqStep);                      // Initialize TimerOne library for the freq we need
  Timer1.attachInterrupt(AC_control, freqStep); 
  

  attachInterrupt(digitalPinToInterrupt(LCD_interupt), LCD_control, RISING); //enabling interrupt for Bnt for LCD
  attachInterrupt(digitalPinToInterrupt(zero_interrupt), zero_crossing, RISING);   //enabling interrupt for zero crossing
}

void loop() {
  Setpoint = SetTEMP;
  long currentTime = millis();
    /* This is event 1 */
  if(currentTime - previousTime_1 >= evenTime_1_Temp){
    TEMP = Get_temp();

    /*Update the timing for the next event*/
    previousTime_1 = currentTime;
  }

      /* This is event 2 */
  if(currentTime - previousTime_2 >= evenTime_2_Hum){
   Humidity = Get_humidity ();

    /*Update the timing for the next event*/
    previousTime_2 = currentTime;
  }

      /* This is event 3 */
  if(currentTime - previousTime_3 >= evenTime_3_PID){
     Input = TEMP;

     myPID.Compute();
     
     dim = map(Output, 0, 255, 128, 0);


     //Serial.print(" PID output: ");
     //Serial.print(dim);
     //Serial.print(";");
 
    /*Update the timing for the next event*/
    previousTime_3 = currentTime;
  }
  
    /* This is event 4 */
  if(currentTime - previousTime_4 >= evenTime_4_Display){
    mainMenuDraw(TEMP,Humidity,map(dim, 128,0,0,128));

    /*Update the timing for the next event*/
    previousTime_4 = currentTime;
  }
  
 
} // end of main

float Get_temp(){
  float temp;
  temp = thermometer_temperature();
  return temp;
}

float Get_humidity (){
  float humidity;
  humidity = humiditySensor_humidity();
  return humidity;
}

void mainMenuDraw(float x, float y, int pid) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Temp:");
  lcd.setCursor(5, 0);
  lcd.print(x);

  lcd.setCursor(12, 0);
  lcd.print("PID:");
  lcd.setCursor(12, 1);
  lcd.print(pid);  
  
  lcd.setCursor(0, 1);
  lcd.print("Hum:");
  lcd.setCursor(5, 1);
  lcd.print(y);
}

void zero_crossing(){

//some delay of AC control// this help to void of fake AC on while loading or resseting  
if(millis() < 10000UL){
   return;
       }
       else{
  zero_cross = true;               // set the boolean to true to tell our dimming function that a zero cross has occured
  i=0;
  digitalWrite(triac, LOW);       // turn off TRIAC
  
  //Input = TEMP;
  // myPID.Compute();
  //analogWrite(triac, Output);
  //Serial.println(Output);
 /*
  bright=analogRead(pot);
  bright=map(bright,0,1023,0,10000);
  delayMicrseconds(bright);
  digitalWrite(triac, HIGH);
  delayMicroseconds(100);
  digitalWrite(triac, LOW);
*/
       }
}
void AC_control(){
    if(zero_cross == true) {              
    if(i>=dim) {                     
      digitalWrite(triac, HIGH); // turn on light    
      i=0;  // reset time step counter                         
      zero_cross = false; //reset zero cross detection
    } 
    else {
      i++; // increment time step counter 
                          
    }                                
  } 
  else{
    return;
  }
  
}

void LCD_control(){
  int activeButton = 0;
  //unsigned long currentTime1 = millis();
  //SettingsTime = currentTime1 + 1000UL;
  lcd.clear();

    for(int i = 0; i < 100; i++){ 
    //while (activeButton == 0) {
    int button = 0;
     Serial.print(" Value of i is: ");
     Serial.println(i);
     if( i >= 99){
     activeButton = 1;
     }
 

    
    readKey = analogRead(0);
    button = evaluateButton(readKey);
    switch (button) {
      case 0: // When button returns as 0 there is no action taken
         
         lcd.setCursor(0, 0);
         lcd.print("SET Temperature:");
         lcd.setCursor(1, 1);
         lcd.print(SetTEMP);

        break;

    //RightBtn//
      case 1:
         lcd.clear();
         lcd.setCursor(0, 0);
         lcd.print("RightBtn:");
         delay(3000);
        break;  

    //UptBtn//  
      case 2:
         //lcd.clear();
         //lcd.setCursor(0, 0);
         //lcd.print("UptBtn:");

         SetTEMP = SetTEMP + 0.1;
         if (SetTEMP >= 39){
          SetTEMP = 38.9;
         }
         lcd.setCursor(1, 1);
         lcd.print(SetTEMP);
         delay(50000);
        break;
           
    //DowntBtn//
      case 3:
         //lcd.clear();
         //lcd.setCursor(0, 0);
         //lcd.print("DowntBtn:");
         SetTEMP = SetTEMP - 0.1;
         lcd.setCursor(1, 1);
         lcd.print(SetTEMP);
         delay(50000);
        break;  

    //LeftBtn//
      case 4:
         lcd.clear();
         lcd.setCursor(0, 0);
         lcd.print("LeftBtn:");
         delay(3000);
        break;  

    //ConfirmBtn//
      case 5:
         lcd.clear();
         lcd.setCursor(0, 0);
         lcd.print("ConfirmBtn:");
         delay(1000);
         activeButton = 1;
        break;                      
    }
  }
}

  int evaluateButton(int x) {
  int result = 0;
  if (x < 60) {
    result = 1; // right
  } else if (x < 195) {
    result = 2; // up
  } else if (x < 380) {
    result = 3; // down
  } else if (x < 590) {
    result = 4; // left
  } 
    else if (x < 790) {
    result = 5; // left
  } 
  return result;
}
