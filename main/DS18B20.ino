// Include the libraries we need

#include <DallasTemperature.h>

// Data wire is plugged into port 13 on the Arduino
#define ONE_WIRE_BUS 13

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

bool thermometer_logEnabled = true;
void thermometer_setLogEnabled(bool logging) {
  thermometer_logEnabled = logging;
}

void thermometer_setup() {
  
  // Start up the library
  sensors.begin();
}

float thermometer_temperature() {
  if (thermometer_logEnabled) {
    //Serial.println("thermometer: Getting temperature...");
  }
  
  // call sensors.requestTemperatures() to issue a global temperature 
  // request to all devices on the bus
  if (thermometer_logEnabled) {
    //Serial.println("thermometer: Requesting temperatures...");
  }
  sensors.requestTemperatures(); // Send the command to get temperatures

  // After we got the temperatures, we can print them here.
  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
  if (thermometer_logEnabled) {
    //Serial.print("thermometer: Temperature for the device 1 (index 0) is: ");
  }
  float temperature = sensors.getTempCByIndex(0);
  if (thermometer_logEnabled) {
    Serial.print(" DS18B20 : ");
    Serial.print(temperature);
    Serial.print(";");
  }
    
  return temperature;
}
